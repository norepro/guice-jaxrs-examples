package org.mpierce.guice.jaxrs.common;

import com.google.inject.Inject;
import com.google.inject.Singleton;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

/**
 * This resource is instantiated once and re-used.
 */
@Path("resource")
@Singleton
public class SampleResource {

    private final RandomSource randomSource;

    @Inject
    SampleResource(RandomSource randomSource) {
        this.randomSource = randomSource;
    }

    @GET
    public String get() {
        return "some data from object with hashCode " + hashCode();
    }

    @GET
    @Path("now")
    @Produces(MediaType.APPLICATION_JSON)
    public JsonPojo getNow() {
        return new JsonPojo();
    }

    @GET
    @Path("subresource")
    public Response getSubresource() {
        // occasionally return a different status code
        if (randomSource.flipCoin()) {
            return Response.status(Response.Status.CONFLICT).entity("conflict").build();
        }

        return Response.ok().entity("ok").build();
    }
}
